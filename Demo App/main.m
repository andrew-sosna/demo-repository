//
//  main.m
//  Demo App
//
//  Created by Andrii Sosna on 12/1/14.
//  Copyright (c) 2014 bMuse LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
