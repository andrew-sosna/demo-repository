//
//  ViewController.m
//  Demo App
//
//  Created by Andrii Sosna on 12/1/14.
//  Copyright (c) 2014 bMuse LLC. All rights reserved.
//

#import "ViewController.h"
@import AVFoundation;

@interface ViewController ()

@end

@implementation ViewController
{
    UIAlertController *_alertController;
    AVSpeechSynthesizer *_synthesizer;
    AVSpeechUtterance *_utterance;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _synthesizer = [[AVSpeechSynthesizer alloc] init];
    
    
    _alertController = [UIAlertController alertControllerWithTitle:@"Слава нації!"
                                                           message:@"Смерть ворогам!"
                                                    preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Саме так"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         NSLog(@"Normalny potsan detected");
														 
														 _utterance = [[AVSpeechUtterance alloc] initWithString:@"Nice choice, mate!"];
														 _utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-AU"];
														 
														 [_synthesizer speakUtterance:_utterance];
														 NSLog(@"Speech was spoken");
														 
                                                     }];
    [_alertController addAction:okAction];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Не понимаю ваш язык"
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action) {
                                                             NSLog(@"Moskal detected");
															 
															 _utterance = [[AVSpeechUtterance alloc] initWithString:@"Ла ла ла ла ла ла ла ла!"];
															 _utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"ru-RU"];
															 
                                                             
                                                             [_synthesizer speakUtterance:_utterance];
															 NSLog(@"Speech was spoken");
                                                             
                                                         }];
    [_alertController addAction:cancelAction];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
	NSLog(@"didReceiveMemoryWarning");
}

- (IBAction)onFlagTap:(UITapGestureRecognizer *)sender
{
    if (self.presentedViewController == _alertController) {
        return;
    }
    [self presentViewController:_alertController
                       animated:YES
                     completion:nil];
	
	NSLog(@"onFlagTap");
 
}

@end
